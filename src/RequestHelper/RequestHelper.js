import Axios from "axios";

const requestWithToken = Axios.create();

export function restRequest(method = "GET", path = "", data = {}) {
  return requestWithToken({
    method: method,
    url: path,
    data: data,
  }).then((res) => {
    return res.data;
  });
}
