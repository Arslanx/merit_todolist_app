import logo from "./logo.svg";
import "./App.css";
import TodoList from "./TodoList/ToDoList";

function App() {
  return (
    <div className="App">
      <TodoList />
    </div>
  );
}

export default App;
