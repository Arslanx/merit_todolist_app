import "./Modal.css";

const Modal = ({ handleClose, show, children }) => {
  const showHideClassName = show ? "modal display-block" : "modal display-none";

  return (
    <div className={showHideClassName}>
      <div className="modal-main">
        <button className="close_btn" type="button" onClick={handleClose}>
          Close
        </button>
        {children}
      </div>
    </div>
  );
};
export default Modal;
