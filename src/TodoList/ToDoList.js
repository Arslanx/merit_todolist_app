import React, { Component, Fragment } from "react";
import axios from "axios";
import "../App.css";
import { restRequest } from "../RequestHelper/RequestHelper";
import { BaseURL } from "../RequestHelper/Base_Url";
import Modal from "../Components/Modal";

class TodoList extends Component {
  state = {
    data: [],
    title: "",
    description: "",
    showEditModal: false,
    selectedTodo: null,
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    restRequest("GET", BaseURL).then((res) => {
      this.setState({ data: res });
    });
  }
  handledelteTodo = (id) => {
    restRequest("DELETE", `${BaseURL}/${id}`).then((res) => {
      alert("deleted sucessfully");
      this.fetchData();
    });
  };

  handleFormSubmit = () => {
    if (this.state.title === "" || this.state.description === "") {
      alert("Please dont leave any field blank");
      return;
    }
    restRequest("POST", BaseURL, {
      title: this.state.title,
      description: this.state.description,
    }).then((res) => {
      res && alert("To Created sucessfully");
      document.getElementById("todo_title").value = "";
      document.getElementById("todo_description").value = "";
      this.setState({ ...this.state, title: "", description: "" });
      this.fetchData();
    });
  };

  handleEditFormSubmit = () => {
    if (this.state.title === "" || this.state.description === "") {
      alert("Please dont leave any field blank");
      return;
    }
    restRequest("PUT", `${BaseURL}/${this.state.selectedTodo.id}`, {
      title: this.state.title,
      description: this.state.description,
    }).then((res) => {
      res && alert("To Updated sucessfully");
      this.setState({ ...this.state, title: "", description: "" });
      this.fetchData();
      this.hideModal();
    });
  };

  showModal = (todo) => {
    this.setState({ ...this.state, show: true, selectedTodo: todo });
  };

  hideModal = () => {
    this.setState({ ...this.state, show: false });
  };

  renderEditForm = () => {
    return (
      <div className="float-left w-100 form_details">
        <label className="float-left w-100">Title</label>
        <input
          type="text"
          name="title"
          id="title"
          value={this.state.title}
          placeholder="Enter Title"
          onChange={(e) =>
            this.setState({
              ...this.state,
              title: e.target.value,
            })
          }
        />
        <label className="float-left w-100">Text</label>
        <input
          type="text"
          id="description"
          name="title"
          value={this.state.description}
          placeholder="Enter Text"
          onChange={(e) =>
            this.setState({
              ...this.state,
              description: e.target.value,
            })
          }
        />
        <button
          className="submit"
          type="button"
          onClick={this.handleEditFormSubmit}
        >
          Submit
        </button>
      </div>
    );
  };

  renderEditModal = () => {
    return (
      <Modal show={this.state.show} handleClose={this.hideModal}>
        {this.renderEditForm()}
      </Modal>
    );
  };
  render() {
    return (
      <div>
        {this.renderEditModal()}
        <div className="w-100 paddingx_20">
          <div className="container">
            <div className="float-left w-100 header_todo">
              <div className="line_height_2 font_weight_500">TODO App</div>
            </div>
            <div className="w-100 border_bottom paddingx_20">
              <div className="container">
                <div className="w-100">
                  <form className="float-left w-100">
                    <div className="heading w-100">TO DO Form</div>
                    <div className="w-100 form_details">
                      <label className="float-left w-100">Title</label>
                      <input
                        type="text"
                        name="title"
                        id="todo_title"
                        placeholder="Enter Title"
                        onChange={(e) =>
                          this.setState({
                            ...this.state,
                            title: e.target.value,
                          })
                        }
                      />
                      <label className="float-left w-100">Text</label>
                      <input
                        type="text"
                        id="todo_description"
                        name="title"
                        placeholder="Enter Text"
                        onChange={(e) =>
                          this.setState({
                            ...this.state,
                            description: e.target.value,
                          })
                        }
                      />
                      <button
                        className="submit"
                        type="button"
                        onClick={this.handleFormSubmit}
                      >
                        Submit
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div style={{ justifyContent: "space-evenly" }}>
              {this.state.data.map((todo, index) => (
                <div
                  key={index}
                  className="todo_card float-left"
                  style={{ width: "18rem" }}
                >
                  <div className="todo_card_body  w-100">
                    <div className="heading_card  w-100">{todo.title}</div>
                    <p className="todo_card_text  w-100">{todo.description}</p>
                    <div className=" w-100 display_flex px_20">
                      <button
                        className="btn-info mr_15"
                        onClick={() => this.showModal(todo)}
                      >
                        Edit
                      </button>
                      <button
                        className="btn-danger"
                        onClick={() => this.handledelteTodo(todo.id)}
                      >
                        Delete
                      </button>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TodoList;
